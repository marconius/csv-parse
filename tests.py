import csv
from unittest import TestCase

from csv_parse import DictReader


class AcceptanceTestCase(TestCase):

    def test_reader_headers_like_standard_csv(self):
        with open('test_data/test.csv') as f:
            std_reader_fieldnames = csv.DictReader(f).fieldnames
            f.seek(0)
            my_reader_fieldnames = DictReader(f).fieldnames

        self.assertEqual(std_reader_fieldnames, my_reader_fieldnames)

    def test_reader_rows_like_standard_csv(self):
        with open('test_data/test.csv') as f:
            expected_objects = [row for row in csv.DictReader(f)]
            f.seek(0)
            objects = [row for row in DictReader(f)]

        self.assertEqual(objects, expected_objects)

    def test_reader_works_like_standard_csv_2(self):
        with open('test_data/test2.csv', encoding='cp1252') as f:
            expected_objects = [row for row in csv.DictReader(f)]
            f.seek(0)
            objects = [row for row in DictReader(f)]

        self.assertEqual(objects, expected_objects)


def get_rows(csv_file):
    with open(csv_file) as f:
        reader = DictReader(f)
        rows = [row for row in reader]
    return rows


class DictReaderTestCase(TestCase):

    def test_reader_constructs_multiple_non_data_fieldnames(self):
        f = open('test_data/headers.csv', 'r')
        self.addCleanup(f.close)
        reader = DictReader(f)

        self.assertEqual(reader.fieldnames, ['year', ' month', ' city'])

    def test_reader_iteration(self):
        rows = get_rows('test_data/iteration.csv')

        self.assertEqual(len(rows), 2)

    def test_reader_rows(self):
        rows = get_rows('test_data/one_col.csv')

        self.assertEqual(rows, [{'col23': 'howdy'}, {'col23': 'world!'}])

    def test_reader_multiple_columns_and_rows(self):
        rows = get_rows('test_data/multi_row_col.csv')

        self.assertEqual(rows, [{'col23': 'Andy', 'col45': '123'}, {'col23': 'world!', 'col45': '222'}])

    def test_reserved_characters_in_data(self):
        rows = get_rows('test_data/multi_row_col_special_chars.csv')
        self.assertEqual(
            rows,
            [
                {"col23": "Andy", "col45": "123", "col55": "hi, there\nyou!"},
                {"col23": "world!", "col45": "222", "col55": "good to see you"}
            ])

    def test_escaped_quoted(self):
        rows = get_rows('test_data/escaped_quotation.csv')

        self.assertEqual(rows, [{"message": 'howdy "super" girl'}])

    def test_empty_lines(self):
        rows = get_rows('test_data/empty_lines.csv')

        self.assertEqual(rows, [{"id": '1', "name": "MArco"}])

    def test_empty_values(self):
        rows = get_rows('test_data/empty_value.csv')

        self.assertEqual(rows, [{"something": "Andy", "other": '', "more": "picture"}])
