import re


DONE_READING = 1
IN_FIELD = 2
IN_QUOTED_FIELD = 3
START_FIELD = 4
START = 5

    
class DictReader():

    def __init__(self, csvfile):
        self.fieldnames = [re.sub('(^\s?")|("\s?$)', '', name) for name in csvfile.readline().rstrip('\n').split(',')]
        self.fd = csvfile
        self.state = START

    def __next__(self):
        possible_escaped_quote = False

        record = {}
        fieldname_index = 0
        value = ''

        if self.state == DONE_READING:
            raise StopIteration

        while True:
            char = self.fd.read(1)
            
            if self.state == START:
                if not char:
                    # Empty line and end of file
                    raise StopIteration
                if char in '\n':
                    # Empty line
                    pass
                elif char == ',':
                    # Empty field
                    record[self.fieldnames[fieldname_index]] = value
                    value = ''
                    fieldname_index += 1
                elif char == '"':
                    self.state = IN_QUOTED_FIELD
                    possible_escaped_quote = True
                else:
                    value += char
                    self.state = IN_FIELD

            elif self.state == IN_QUOTED_FIELD:
                if char == '"':
                    self.state = IN_FIELD
                else:
                    value += char

            elif self.state == IN_FIELD:
                if possible_escaped_quote and char == '"':
                    value += char
                    self.state = IN_QUOTED_FIELD
                elif char in ',\n' or not char:
                    record[self.fieldnames[fieldname_index]] = value
                    value = ''
                    fieldname_index = fieldname_index + 1 if char == ',' else 0
                    self.state = START if char else DONE_READING
                    if char == '\n' or not char: break
                else:
                    value += char

        return record

    def __iter__(self):
        return self
