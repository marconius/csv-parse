Csv-Parse
---------

Thank you for taking the time to evaluate my csv parser! To verify the solution, simply run the tests:

```bash
$ python3 -m unittest tests
```


Following is a list of assumptions I made in an attempt to save time:

* files are encoded in one-byte character encodings (this assumption could easily be removed by reading whole lines
  and then iterating over the resulting string instead of reading one character at a time.)
* field names never contain new lines or special quotation marks
* there is never any space between a separator and the beginning of a quoted field


References
----------

* Lutz, Mark. _Python Pocket Guide_ (4th ed.)
* [python csv documentation][csv_doc]
* [Source code for csv standard library][csv_lib]
* [Windows-1252 on Wikipedia][cp1252] (Took me a while to find the right encoding! Probably because I haven't used
  Windows in years.

[csv_doc]: https://docs.python.org/3.6/library/csv.html
[csv_lib]: https://github.com/python/cpython/blob/3.6/Modules/_csv.c#L601
[cp1252]: https://en.wikipedia.org/wiki/Windows-1252
